/**
 *
 * # Module S3 Bucket
 *
 * ## Main goals
 *
 * Amazon Simple Storage Service (Amazon S3) is an object storage service that stores data as objects within buckets. You can use Amazon S3 to store and retrieve any amount of data at any time.
 * This module creates a S3 bucket.
 *
 * See also:
 *
 * * [Terraform Documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket)
 * * [AWS Documentation](https://docs.aws.amazon.com/ec2/)
 *
 * Sample usage :
 * ```
 * module "s3" {
 *    source = "git::https://git./terraform-aws-s3.git?ref=2.1.0"
 *    security = "zone"
 *    environment = "dev"
 *    company = "entreprise"
 *    service_id = "axe
 *    name = "sample"
 * }
 *
 * ```
 */

resource "aws_s3_bucket" "bucket_instance" {
  bucket        = local.aws_s3_bucket_bucket_instance_name
  force_destroy = var.force_destroy ? true : false

  tags = merge(local.required_tags, var.tags, {
    Name = local.long_name
  })
}

resource "aws_s3_bucket_public_access_block" "block" {
  bucket                  = aws_s3_bucket.bucket_instance.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.bucket_instance.id
  versioning_configuration {
    status = var.versioning == true ? "Enabled" : "Suspended"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "custom_bucket_encryption" {
  bucket = aws_s3_bucket.bucket_instance.id
  rule {
    bucket_key_enabled = false
    apply_server_side_encryption_by_default {
      kms_master_key_id  = var.encryption_type == "kms" ? (var.kms_key_arn != null ? var.kms_key_arn : "aws/s3") : null
      sse_algorithm      = local.encryption_type_mapping[var.encryption_type]
    }
  }
}
