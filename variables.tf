variable "environment" {
  type        = string
  description = "The environment name: plg/dev/npd/ppd/prd."
  # validation {
  #   condition     = contains(["plg", "dev", "npd", "ppd", "prd"], var.environment)
  #   error_message = "The environment must be a valide value or empty: plg/dev/npd/ppd/prd."
  # }
}

variable "company" {
  type        = string
  description = "The trigram of the company."
  # validation {
  #   condition     = length(var.company) == 3
  #   error_message = "The company is mandatory and must be a trigram."
  # }
  # validation {
  #   condition     = var.company == lower(var.company)
  #   error_message = "The company must be in lowercase."
  # }
}

variable "service_id" {
  type        = string
  description = "The service portfolio or serviceID."
  # validation {
  #   condition     = length(var.service_id) <= 50
  #   error_message = "The serviceID must be a less than 50 characters."
  # }
  # validation {
  #   condition     = var.service_id == lower(var.service_id)
  #   error_message = "The serviceID must be in lowercase."
  # }
}

variable "security" {
  type        = string
  description = "The zone of the account"
  # validation {
  #   condition     = var.security == lower(var.security)
  #   error_message = "The subnet zone must be in lowercase."
  # }
  # validation {
  #   condition     = can(regex("^z[0-2]?[0-9]$", var.security))
  #   error_message = "The subnet zone must be in format: zi (^z[0-2]?[0-9]$)."
  # }
}

variable "name" {
  type        = string
  description = "The bucket name (only lowercase characters and number)"
  # validation {
  #   condition     = can(regex("^[a-z0-9]*$", var.name))
  #   error_message = "The bucket name must only contains lowercase characters or number (^[a-z0-9]*$)."
  # }
}

variable "versioning" {
  type        = bool
  description = "Weather or not the versioning is enabled."
  default     = false
}

variable "encryption_type" {
  type        = string
  description = "The encrytion type: s3/kms."
  default     = "kms"
  # validation {
  #   condition     = contains(["s3", "kms"], var.encryption_type)
  #   error_message = "The encrytion type must be a valide value: s3/kms."
  # }
}

variable "kms_key_arn" {
  type        = string
  description = "A custom kms key arn used for encrytion."
  default     = null
}

locals {
  validate_kms_encrytion_condition_fail = var.kms_key_arn != null && var.encryption_type != "kms"
  validate_kms_encrytion_error_message  = "You cannot specify a custom kms key with s3 as encrytion type."
  validate_kms_encrytion_chk = regex(
    "^${local.validate_kms_encrytion_error_message}$",
    (!local.validate_kms_encrytion_condition_fail
      ? local.validate_kms_encrytion_error_message
  : ""))
}

variable "tags" {
  type        = map(any)
  description = "A map of tags to assign to the resource."
  default     = {}
}

variable "force_destroy" {
  type        = bool
  description = "A boolean that indicates all objects (including any locked objects) should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable."
  default     = false
}
