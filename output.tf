output "id" {
  value       = aws_s3_bucket.bucket_instance.bucket
  description = "The full bucket name."
}

output "arn" {
  value       = aws_s3_bucket.bucket_instance.arn
  description = "The arn of the created bucket."
}
