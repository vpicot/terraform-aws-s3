
# Module S3 Bucket

## Main goals

Amazon Simple Storage Service (Amazon S3) is an object storage service that stores data as objects within buckets. You can use Amazon S3 to store and retrieve any amount of data at any time.
This module creates a S3 bucket.

See also:

* [Terraform Documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket)
* [AWS Documentation](https://docs.aws.amazon.com/ec2/)

Sample usage :
```
module "s3" {
   source = "git::https://git./terraform-aws-s3.git?ref=2.1.0"
   security = "zone"
   environment = "dev"
   company = "entreprise"
   service_id = "axe"
   name = "sample"
}

```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.0.0 |
| aws | ~> 4.0 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_s3_bucket.bucket_instance](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_public_access_block.block](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.custom_bucket_encryption](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_s3_bucket_versioning.versioning](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| company | The trigram of the company. | `string` | n/a | yes |
| encryption\_type | The encrytion type: s3/kms. | `string` | `"kms"` | no |
| environment | The environment name: plg/dev/npd/ppd/prd. | `string` | n/a | yes |
| force\_destroy | A boolean that indicates all objects (including any locked objects) should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable. | `bool` | `false` | no |
| kms\_key\_arn | A custom kms key arn used for encrytion. | `string` | `null` | no |
| name | The bucket name (only lowercase characters and number) | `string` | n/a | yes |
| security | The zone of the account | `string` | n/a | yes |
| service\_id | serviceID. | `string` | n/a | yes |
| tags | A map of tags to assign to the resource. | `map(any)` | `{}` | no |
| versioning | Weather or not the versioning is enabled. | `bool` | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| arn | The arn of the created bucket. |
| id | The full bucket name. |


