locals {
  encryption_type_mapping = {
    "kms" = "aws:kms"
    "s3"  = "AES256"
  }
}

locals {
  required_tags = {
    Security    = upper(var.security)
    Environment = upper(var.environment)
    Company     = upper(var.company)
    ServiceID   = upper(var.service_id)
  }

  long_name = "objstr-${var.company}---${var.security}-${var.environment}-${var.service_id}-${data.aws_caller_identity.current.account_id}-${var.name}"
  short_name = "objstr-${var.company}---${var.security}-${var.environment}--${data.aws_caller_identity.current.account_id}-${var.name}"
  aws_s3_bucket_bucket_instance_name = length(local.long_name) > 63 ? local.short_name : local.long_name
}

locals {
  validate_bucket_name_length_condition_fail = length(local.short_name) > 63
  validate_bucket_name_length_error_message  = "The bucket name is too long, ${length(local.short_name)} > 63."
  validate_bucket_name_length_chk = regex(
    "^${local.validate_bucket_name_length_error_message}$",
    (!local.validate_bucket_name_length_condition_fail
      ? local.validate_bucket_name_length_error_message
  : ""))
}
